<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            // Order id
            $table->increments('id');

            // User id
            $table->unsignedInteger('user_id');

            // Driver id
            $table->unsignedInteger('driver_id')->nullable();

            // Delivery details
            $table->string('delivery_address');
            $table->date('delivery_date');
            $table->time('delivery_time_start');
            $table->time('delivery_time_end');

            // Timestamps
            $table->timestamps();

            // Keys
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
