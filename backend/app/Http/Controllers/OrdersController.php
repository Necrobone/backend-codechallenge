<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrdersController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // Create the param validation
            $validation = Validator::make($request->all(), [
                'user_id'             => 'required|integer|max:11',
                'delivery_address'    => 'required|string|max:255',
                'delivery_date'       => 'required|date|date_format:Y-m-d|after_or_equal:today',
                'delivery_time_start' => 'required|date_format:H:i:s|before:delivery_time_end',
                'delivery_time_end'   => 'required|date_format:H:i:s|after:delivery_time_start|before_or_equal:' .
                    date('H:i:s', strtotime($request->delivery_time_start . ' +8 hours')),
            ], [
                'delivery_time_end.before_or_equal' => 'The :attribute must be a date before or equal to 8 hours after delivery time start.'
            ]);

            // Validation error
            if ($validation->fails()) {
                return response()->json(['error' => $validation->errors()], 400);
            }

            // Store the order transaction
            DB::transaction(function () use ($request) {
                // Create new order
                $order = new Order;

                // Set the order data
                $order->user_id = $request->user_id;
                $order->delivery_address = $request->delivery_address;
                $order->delivery_date = $request->delivery_date;
                $order->delivery_time_start = $request->delivery_time_start;
                $order->delivery_time_end = $request->delivery_time_end;

                // Save the order
                $order->save();

                // Assign a random driver
                $order->driver_id = User::where('id', '<>', $request->user_id)->inRandomOrder()->first()->id;

                // Save the driver
                $order->save();
            });
        } catch (\Exception $exception) {
            // Return the exception
            return response()->json(['error' => $exception->getMessage()], 400);
        }

        // Return
        return response()->json(['success' => 'Order created successfully'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // Create the param validation
        $validation = Validator::make($request->all(), [
            'driver_id'     => 'required|integer|max:11',
            'delivery_date' => 'required|date|date_format:Y-m-d',
        ]);

        // Validation error
        if ($validation->fails()) {
            return response()->json(['error' => $validation->errors()], 400);
        }

        // Find the driver's orders
        $order = Order::where(['driver_id' => $request->driver_id, 'delivery_date' => $request->delivery_date])->with([
            'user',
            'driver'
        ])->get();

        // Return
        return $order->isEmpty() ? response()->json(['error' => 'Order not found'], 404) : $order;
    }
}
