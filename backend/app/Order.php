<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'driver_id', 'delivery_address', 'delivery_date', 'delivery_time_start', 'delivery_time_end',
    ];

    /**
     * Get the user that owns the order.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the driver that owns the order.
     */
    public function driver()
    {
        return $this->belongsTo('App\User', 'driver_id');
    }
}
