<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email',
    ];

    /**
     * Get the user orders.
     */
    public function orders()
    {
        return $this->hasMany('App\Order', 'user_id');
    }

    /**
     * Get the driver deliveries.
     */
    public function deliveries()
    {
        return $this->hasMany('App\Order', 'driver_id');
    }
}
